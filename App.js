import React from 'react';
import {View, StyleSheet} from 'react-native';
import Header from './components/Header';
import ListItem from './components/ListItem';

const App = () => {
  return (
    <View style={styles.container}>
      <Header/>
      <ListItem/>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
