import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Header = () => {
    return(
        <View style={styles.header}>
            <View>
                <Text style={styles.title}>TURISMO BOLIVIA</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: 60,
        flexDirection: 'row',
        backgroundColor: 'deepskyblue',
        alignItems: 'center',
        paddingLeft: 10,
    },
    title: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
});

export default Header;