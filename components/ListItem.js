import React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';

const ListItem = () => {
    return (
        <View style={styles.item}>
            <Image style={styles.image} source={{uri: 'https://s3-us-west-2.amazonaws.com/denomades/blog/wp-content/uploads/2015/04/05163420/salar-de-uyuni-bolivia.jpg'}} />
            <View style={styles.container}>
                <View>
                    <Text style={styles.textTitle}>Salar de Uyuni</Text>
                    <Text style={styles.textSubt}>Bolivia</Text>
                </View>
                <View>
                    <Text style={styles.conText}>El salar de Uyuni es el mayor desierto de sal continuo y alto del mundo,
                        con una superficie de 10 582 km² (o 4085 millas cuadradas). Está situado a
                        unos 3650 msnm en el suroeste de Bolivia, en la provincia de Daniel Campos,
                        en el departamento de Potosí, dentro de la región altiplánica de la cordillera de los Andes.
                        El salar de Uyuni es una importante reserva de litio e igualmente cuenta con
                        importantes cantidades de potasio, boro y magnesio.
                    </Text>
                </View>
            </View>
            <TouchableOpacity onPress={() => boton('Esto es una prueba')}>
                <View style={styles.boton}>
                    <Text style={styles.btnText}>Prueba</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
}

const boton = (prueba) => {
    console.log(prueba);
};

const styles = StyleSheet.create({
    image: {
        height: 300,
        width: '100%',
    },
    item: {
        width: '100%',
        height: 120,
        padding: 10,
    },
    textTitle: {
        fontWeight: 'bold',
        fontSize: 25,
    },
    textSubt: {
        color: 'gray',
        fontSize: 20,
    },
    container: {
        margin: 10,
    },
    conText: {
        paddingTop: 20,
        fontSize: 20,
    },
    boton: {
        height: 40,
        width: 80,
        backgroundColor: 'deepskyblue',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: "flex-end",
    },
    btnText: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
    }
});

export default ListItem;



